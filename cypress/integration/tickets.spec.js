describe("Tickets", () => {
    beforeEach(() => {
        cy.visit('https://ticket-box.s3.eu-central-1.amazonaws.com/index.html')
    });

    it("fills all the text input fields", () => {
        const firstName = "Marcelo";
        const lastName = "Chazan";

        //Text field
        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("marcelo.chazan@gmail.com");
        cy.get("#requests").type("Carnivoro");
        cy.get("#signature").type(firstName + " " + lastName);

    });

    it("select two tickets", () => {
        //Select Field
        cy.get("#ticket-quantity").select("2");
    });

    it("select 'vip' ticket type", () => {
        //Radio Button
        cy.get("#vip").check();
    });

    it("selects 'social media' checkbox", () => {
        //Checkbox
        cy.get("#social-media").check();
    });

    it("selects 'friend', and 'publication', then uncheck 'friend'", () => {
        //Check and unCheck
        cy.get("#friend").check();
        cy.get("#publication").check();
        cy.get("#friend").uncheck();
    });

    it("has 'TICKETBOX' header's heading", () => {
        //Assertions
        //Contém
        cy.get("header h1").should("contain", "TICKETBOX");

    });

    it("alerts on invalid email", () => {
        //Assertions com alias(as)
        cy.get("#email")
            .as("email")
            .type("marcelo.chazan-gmail.com");
        //Existe
        cy.get("#email.invalid")
            .should("exist");

        cy.get("@email")
        .clear()
        .type("marcelo.chazan@gmail.com");
        //Não existe
        cy.get("#email.invalid").should("not.exist");
        
    });

    it("fills and reset the from", () => {
        const firstName = "Marcelo";
        const lastName = "Chazan";
        const fullName = firstName + " " + lastName;

        //Text field
        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("marcelo.chazan@gmail.com");
        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").check();
        cy.get("#friend").check();
        cy.get("#requests").type("IPA beer");

        cy.get(".agreement p").should("contain", 
            "I, " + fullName + ", wish to buy 2 VIP tickets");

        cy.get("#agree").check();
        cy.get("#signature").type(fullName);

        cy.get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled");

        cy.get("button[type='reset']").click();
        cy.get("@submitButton").should("be.disabled");
    });

    it("fills mandatory fields using support commnad", () => {
        const customer = {
            firstName: "Marcelo",
            lastName: "Chazan",
            email: "marcelo.chazan@gmail.com"
        };

        cy.fillMandatoryFields(customer);

        cy.get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled");

        cy.get("#agree").uncheck();

        cy.get("@submitButton").should("be.disabled");
    });
});